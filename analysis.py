"""Main file to run spectral analysis."""
import numpy as np

from .spectra_modification import shuffle_spectrum, convert_spectral_units
from .spectra_analysis import analyze_spectrum
from .estimate_redshift import estimate_redshift
from .constants import lambda_lya


def _collect_results(sqlst, percentiles=[16.0, 50.0, 84.0]):
    """Computes percentiles."""
    rdict = {}
    allkeys = []
    for csq in sqlst:
        for k in csq.keys():
            if k not in allkeys:
                allkeys.append(k)

    for k in allkeys:
        rdict[k] = np.percentile([i[k] for i in sqlst if k in i], percentiles)
    return rdict


def run_analysis(spec, z, R, z_err=1e-5, estimate_z=False, niter=100,
                 lambda0=lambda_lya,
                 return_original_measure=True,
                 use_random_spectra=True,
                 **kwargs
):
    """Analyse spectra, return spectral quantities computed and the uncertainty.

    Keywords:
    spec           -- [lambda, f_lambda, error(f_lambda)]
    z              -- Redshift of source (see `estimate_z`)
    R              -- Spectral resolution
    z_err          -- 1-sigma error on redshift
    estimate_z     -- Try to estimate systemic redshift of source at each
                      iteration. In this case `z_err` is ignored.
    niter          -- Number of iterations to perform analysis.
    return_original_measure -- If True (default), returns also original measure
                               as first entry.
    use_random_spectra -- If True (default) changes spectrum at every iteration.
    kwargs -- given to analysis

    Returns:
    Dictionary with spectral quantities and redshift estimate as keys and as
    values the [16,50,84]th percentile of the distribution (or: [original_measure, ...])
    if `return_original_measure` is True.
    Dictionary also contains as `[pos/neg]_peak_fraction` the fraction of times
    a positive / negative peak has been detected.
    """
    sqlst = []
    frac_neg_peak = 0.0
    frac_pos_peak = 0.0
    for i in range(niter):
        if use_random_spectra:
            cspec     = shuffle_spectrum(spec, R = R)
            if estimate_z:
                cz, _ = estimate_redshift(cspec, z, R=R)
            else:
                cz = np.random.normal(loc = z, scale = z_err)
        else:
            cspec = spec
            cz = z
        cspec_v = convert_spectral_units(cspec, cz, lambda0)

        cr = analyze_spectrum(cspec_v, **kwargs)
        cr["z"] = cz
        sqlst.append(cr)
        frac_neg_peak += float((cr["R_L_cut_neg"] > 1.0) and (cr["R_F_cut_neg"] > 1.0))
        frac_pos_peak += float((cr["R_L_cut_pos"] > 1.0) and (cr["R_F_cut_pos"] > 1.0))

    frac_pos_peak /= niter
    frac_neg_peak /= niter

    if niter == 1:
        rdict = sqlst[0]
    else:
        rdict = _collect_results(sqlst)

    if return_original_measure:
        orig_dict = run_analysis(
            spec,
            z,
            R,
            z_err=z_err,
            niter=1,
            use_random_spectra=False,
            lambda0=lambda0,
            return_original_measure=False,
            **kwargs
        )
        for k, v in rdict.items():
            if k in orig_dict:
                rdict[k] = [orig_dict[k]] + list(v)
            else:
                rdict[k] = [np.nan] + list(v)
    rdict["pos_peak_fraction"] = frac_pos_peak
    rdict["neg_peak_fraction"] = frac_neg_peak

    return rdict
