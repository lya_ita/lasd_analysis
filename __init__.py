"""Module for spectra analysis.

Default spectrum data format is [x,y,err]
"""

from .analysis import run_analysis
from .spectra_analysis import check_emitter
from .description import DESCRIPTION

