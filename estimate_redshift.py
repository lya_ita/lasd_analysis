"""File to try to estimate the redshift of a spectrum.
"""
import numpy as np
import logging

from .constants import c
from .constants import lambda_lya

from .detect_peaks import detect_peaks
from .spectra_modification import convert_spectral_units
from .spectra_analysis import _get_ymax


def _v_to_z(v, z_old):
    """Shift of velocity to new z"""
    return (v / (c / 1e5) + 1.0) * (z_old + 1)


def estimate_redshift(
    spec0,
    z_approx,
    R,
    lambda0=lambda_lya,
    mode="auto",
    lookahead=120,
    delta_err=1,
    run_peakdetect=True,
    use_linefits=False,
):
    """Returns estimated redshift.

    Parameters
    spec0 : Sequence
        Spectrum in observers units
    z_approx : float
        approximate redshift as given by user
    R : float
        User submitted resolving power
    lambda0 : float
        default lambda_lya
    mode : str
        Can be: `auto` (default)
    lookahead : float

    delta_err : float
    run_peakdetect : bool
    use_linefits : bool
    """
    assert isinstance(z_approx, float), "`z_approx` has to be float. (%s)" %(str(z_approx))
    spec = convert_spectral_units(spec0, z_approx, lambda0)
    if mode != "auto":
        raise ValueError("`mode` not implemented yet.")

    ## Not sure if the peak detection is needed...?
    ## For now it is only good for double peaked spectra with a dominant blue peak
    ## And for this it is a bit expensive...
    ## TODO [MG] I think the best would be to do a clear S/N and / or R cut...
    new_lc = None
    if run_peakdetect:
        new_lc, alg_code = estimate_vshift_peakdetect(spec, lambda0, R=R)
    if new_lc is None:
        new_lc, alg_code = estimate_vshift_descending(
            spec,
            lambda0,
            lookahead=lookahead,
            delta_err=delta_err,
            use_line_fit=use_linefits,
        )

    # Uncertainty due to binning
    dx = spec0[0][1] - spec0[0][0]
    new_lc += np.random.normal(scale=dx)

    z_new = _v_to_z(new_lc, z_approx)
    msg = "Redshift detection failed (z_new = %s, alg_code = %d)" %(str(z_new), alg_code)
    assert isinstance(z_new, float), msg
    assert z_new > 0, msg
    assert z_new < z_approx + 2.0, msg

    return make_sure_float(z_new) - 1, alg_code


def estimate_vshift_descending(
    spec,
    lambda0=lambda_lya,
    v_peak="max",
    ipad=2,
    lookahead=75,
    delta_err=2.5,
    delta_slope=1.0,
    use_line_fit=False,
):
    """Estimates redshift of Lya spectrum by simply using the highest
    peak and then move towards the blue.

    The goal is to find the point when the spectrum is not descending anymore
    which prevents a faulty detection for very wide peaks.

    Returns new lc in `spec[0]` input units.

    `ipad` is padding used on both sides to find peak.

    Parameters
    ----------
    lookahead : float 
        How far to look ahead in the spectrum in km/s. The larger this value, 
        the bluer the detected redshift will be.
    delta_err : float
        How much error to add to each flux. The larger this value the redder the
        detected redshift will be.
    delta_slope :  float
        This one sets how much the error of the slope fitting is taken into account.
        Can be from -1 to 3 or so. Lower values means a faster stop.
    """
    x, y, err = spec
    if v_peak == "max":
        ymax = _get_ymax(y[ipad:-ipad], err[ipad:-ipad])
        v_peak = np.mean(x[y == ymax])

    # Use minimum blueward of central peak
    m_blue = spec[0] < v_peak
    xblue = x[m_blue][::-1]  # flip for convenience
    yblue = y[m_blue][::-1]
    errblue = err[m_blue][::-1]

    dx = x[1] - x[0]
    ila = int(np.ceil(lookahead / dx)) + 1
    logging.debug(
        "Estimate redshift descending blueward of %g (ila = %d).", v_peak, ila
    )

    assert np.sum(m_blue) > 0, "Error. No blueward points found from peak?"

    if use_line_fit:
        course_spectrum = (ila < 10) or (np.sum(m_blue) <= ila)

        if not course_spectrum:
            new_lc, alg_code = polyfit_algorithm(
                xblue, yblue, errblue, m_blue, ila, delta_slope
            )
            if new_lc is not None:
                return new_lc, alg_code

        logging.debug("Very coarse spectrum. Don't use fitting.")
        new_lc, alg_code = walking_algorithm(
            xblue, yblue, errblue, m_blue, ila, delta_err
        )
        if new_lc is not None:
            return new_lc, alg_code

        assert (
            np.sum(m_blue) > ila
        ), "Error in redshift detection. Too few datapoints descending."

    else:
        logging.debug("Very coarse spectrum. Don't use fitting.")
        new_lc, alg_code = walking_algorithm(
            xblue, yblue, errblue, m_blue, ila, delta_err
        )
        if new_lc is not None:
            return new_lc, alg_code

    assert (
        np.sum(m_blue) > ila
    ), "Error in redshift detection. Too few datapoints descending."

    logging.error(
        "Estimating redshift failed. [internal data: v_peak = %g, ila = %d  ]",
        v_peak,
        ila,
    )
    raise Exception("Estimating redshift failed.")


def polyfit_algorithm(xblue, yblue, errblue, m_blue, ila, delta_slope):
    # For better spectra use a linear fit to decide if flux increases again
    # [MG] Larger lookahead needed for this to work!

    try:
        for i in range(np.sum(m_blue) - ila):
            cnorm = np.mean(yblue[i : i + ila]) ## unused..?
            cx = xblue[i : i + ila]
            cy = yblue[i : i + ila]
            cerr = errblue[i : i + ila]
            p, V = np.polyfit(cx, cy, deg=1, w=1.0 / cerr, cov=True)
            perr = np.sqrt(np.diag(V))
            if p[0] + delta_slope * perr[0] < 0:
                ## Stopping at the minimum is possible...
                ## ..and maybe good as the randomness should go away by shuffling
                ## the spectra.
                # cystop = np.min(cy)
                ## But now I use the point once a non-positive slope is found
                cystop = cy[0]
                new_lc = cx[cy == cystop]

                logging.debug(
                    "Stop descending (i=%d, slope=%g, err = %g). New lc: %g",
                    i,
                    p[0],
                    perr[0],
                    new_lc,
                )
                return new_lc, 2
        return None, 2
    except ValueError:
        logging.debug("Error in line fit. Falling back to walking algorithm")
        return None, 2


def walking_algorithm(xblue, yblue, errblue, m_blue, ila, delta_err):
    """ Algorithm for finding the turnaround point of a slope
    Parameters
    ----------
    xblue : array
    yblue : array
    errblue : array
    m_blue : float
        Number of points on blue side of lya
    ila : float
        Lookahead, how large a region should be considered when checking
        for turnaround
    delta_err : float
        multiplicative factor for including the errors
    """
    for i in range(np.sum(m_blue)):
        cx = xblue[i : i + ila]
        cy = yblue[i : i + ila]
        cerr = errblue[i : i + ila]
        if np.min(cy + delta_err * cerr) > yblue[i]:  # Then turnaround is found.
            # This return is problematic -> It will return *all* points where this is true
            # Question is if the first point is actually what we want
            # Changed to first point -- make sure that this works as expected
            return cx[cy == np.min(cy)][0], 3
    return None, 3


def estimate_vshift_peakdetect(spec, lambda0=lambda_lya, R=None):
    """    Returns new lc in `spec[0]` input units.

    Keywords:
    spec      -- [x,y,err] in velocity space
    """
    x, y, err = spec
    assert x[0] < 0 and x[-1] > 0, "Spectrum one-sided. Not in velocity space?"
    m = np.abs(x < 1000.0)

    if R is not None:
        peaks, vals = detect_peaks(
            [x[m], y[m], err[m]],
            R=R,
            smooth_spectrum_width=1,
            smooth_width_unit="R",
        )
    else:
        peaks, vals = detect_peaks([x[m], y[m], err[m]], R=R)
    px, py = peaks
    vx, vy = vals

    logging.debug("Detecting redshift (peakdetect). Found %d peaks at %s and %d valleys at %s.",
                 len(px), str(px), len(vx), str(vx))

    # If two peaks and a valley in the middle. Take the valley.
    if len(px) == 2:
        if len(vx) == 1:
            if px[0] < vx[0] and px[1] > vx[0]:
                logging.debug("Detecting redshift based on valley between two peaks (vx = %s).", str(vx))
                return make_sure_float(vx[0]), 1
        else:
            # If no valley was found this is reasonable (but what if multiple valleys?)
            logging.debug("Detecting redshift based shift from peak at %g." % (px[1]))
            return estimate_vshift_descending(spec, lambda0, v_peak=px[1])

    return None, 1

    # # If more than two peaks, try to find the most significant ones
    # if len(px) > 2:
    #     if len(vx) == 1:
    #         slvl = []
    #     for i, cy in enumerate(py):
    #         dpx = 100. # Typical width of peak to calculate error level
    #         slvl = py / np.median(err[(x < px[i] - dpx) & (x > px[i] + dpx)])


def make_sure_float(value):
    try:
        a = np.asscalar(value)
        return a
    except:
        return value
