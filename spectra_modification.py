"""Functions for modifying spectrum, i.e., they will take a spectrum and return
a new, different one.
"""

import numpy as np
import scipy as sp

from scipy.signal import gaussian

from .constants import lambda_lya

from astropy import units as u
from astropy.constants import c as apy_c
from astropy.cosmology import Planck15


def shuffle_spectrum(spec, sd_smooth=None, R=None, lambda0=lambda_lya):
    """Will shuffle spectrum according to error.

    Keywords:
    spec      -- [x,y,err]
    sd_smooth -- If given new spectrum will be smoothed by Gaussian with this
                 standard deviation.

    Returns:
    [x,y, err] of new spectrum
    """
    if R is not None:
        assert sd_smooth is None
        sd_smooth = (lambda0 / R) / (2 * np.sqrt(2 * np.log(2)))

    x, y, err = spec

    # fix points with large error. otherwise problems with integral values (L_*, ..)
    sd = err.copy()
    sd[err > np.median(err) * 1e3] = np.median(err)

    ynew = np.random.normal(loc=y, scale=sd)

    if sd_smooth is not None:
        return smooth_spectrum([x, ynew, err], width=sd_smooth)
    else:
        return [x, ynew, err]


def get_smoothing_kernel(spec, R, multiplicative_factor):
    """ This calculates the size of the smoothing kernel based on user
    submitted R.
    
    spec : tuple
        Spectrum
    R : float
        User submitted resolution
    multiplicative_factor : float
        Number of R to smooth by.
    """
    res = apy_c.to("km/s").value / R

    v = spec[0]
    index_zero = np.argmin(np.abs(v))
    sampling = np.abs(v[index_zero] - v[index_zero - 1])
    
    pixels_per_resolution_element = res / sampling

    return pixels_per_resolution_element


def smooth_spectrum(
    spec,
    width,
    window_type="gaussian",
    convolve_mode="valid",
    window_len=4.5,
    width_unit="v",
):
    """
    Smooths spectrum with window function.
    
    Keyword Arguments:
    x             -- x-values of spectrum in chosen units
    y             -- y-values of spectrin in whatever units
    width         -- width of smoothing in x-units
    window_type   -- Shape of smoothing/window function (default 'gaussian').
                     Can be:
                        * 'gaussian' - Gaussian window. width is standard dev
    convolve_mode -- mode of `np.convolve`. Default: 'same'
    window_len    -- Length of window in units of `width` (will be modified
                     to be uneven)
    
    Returns:
    [x,y] of smoothed spectrum, also err if given
    """
    if len(spec) == 3:
        x, y, err = spec
    else:
        x, y = spec
        err = None

    if len(x) != len(y):
        raise ValueError("x and y arrays must be equal length")

    if width_unit == "v":
        Deltax = np.abs(x[1] - x[0])
        width_conv = float(width) / Deltax
    elif width_unit == "px":
        width_conv = width
    elif width_unit == 'R':
        width_conv = width
    else:
        raise ValueError('Possible values of "width_unit" are ["v", "px", "R"]')

    ycon = sp.ndimage.gaussian_filter1d(y, width_conv)
    xcon = x
    if err is not None:
        errcon = err

    """
    if window_type == "gaussian":
        window_len = np.ceil(window_len * width_conv)
        window_len += window_len % 2 + 1  # make window length uneven
        window = gaussian(window_len, width_conv)
    elif window_type == "tophat":
        window_len = np.ceil(width_conv)
        window_len += window_len % 2 + 1  # make window length uneven
        window = np.ones(window_len) / float(window_len)
    else:
        raise ValueError("'window_type' can be 'gaussian'.")

    window /= np.sum(window)

    ycon = np.convolve(window, y, mode=convolve_mode)

    if convolve_mode == "valid":
        s = int(0.5 * (window_len - 1))
        xcon = x[s:-s]
        if err is not None:
            errcon = err[s:-s]
    elif convolve_mode == "same":
        xcon = x
        if err is not None:
            errcon = err
    else:
        raise ValueError(
            "convolve_mode other than 'valid' and 'same' " "not yet implemented"
        )
    """
    if err is None:
        return xcon, ycon
    else:
        return xcon, ycon, errcon


def convert_spectral_units(spec, z, lambda0=lambda_lya, verbose=False, v_cut=2500.0):
    """Converts units of spectrum from (lambda, f_lambda) [observed] to (v, L_v)
    """
    assert z > 0, "Unphysical redshift %g" % (z)

    # decide upon a max/min velocity range to take for Ly-alpha
    vrange = v_cut * u.km / u.second

    # convert the luminosity distance from the redshift, and the area of a
    # sphere of that size
    dl = Planck15.luminosity_distance(z)
    areal = (4.0 * sp.pi * dl ** 2).cgs

    # unpack the spec input
    lam_obs, flam_obs, dflam_obs = spec

    # convert wavelength to velocity, assuming lambda0 is the *OBSERVED*
    # wavelength of Lyman alpha.
    v = (lam_obs / (1 + z) - lambda0) / lambda0 * apy_c.to("km/s")

    #  put l into the restframe, and fluxes to luminosities, correcting for
    #  cosmological 1D dimming
    lam_rest = lam_obs / (1.0 + z)
    llam = flam_obs * areal * (1.0 + z)
    if dflam_obs is not None:
        dllam = dflam_obs * areal * (1.0 + z)

    # convert the per AA to per km/s
    lperv = llam * lam_rest / apy_c.to("km/s")
    if dflam_obs is not None:
        dlperv = dllam * lam_rest / apy_c.to("km/s")

    # take the appropriate range
    m = np.abs(v) < vrange

    # print a couple of integrals to make sure the conversion worked ;-)
    if verbose:
        print(sp.trapz(llam[m], lam_rest[m]).cgs)
        print(sp.trapz(lperv[m], v[m]).cgs)
        print([v[m].to("km/s").value, lperv[m].value, dlperv[m].value])

    if dflam_obs is not None:
        return [v[m].to("km/s").value, lperv[m].value, dlperv[m].value]
    else:
        return [v[m].to("km/s").value, lperv[m].value]


def remove_continuum(spec, cont, xa, xb):
    """ Removes continuum of spectrum and returns fluxes.
    Keyword Arguments:
    spec --
    cont -- continuum level
    """
    x, y, err = spec

    yr = y.copy() - cont
    yr = np.clip(yr, 0, np.inf)
    yr[np.logical_or(x < xa, x > xb)] = 0.0

    # Check before we give it back
    assert not np.any(np.isnan(yr)), "nan created in continuum removal."

    return yr


def preprocess_spectrum(spec):
    """Preprocesses spectrum, i.e.:
           - fixes zero flux & error (masked) pixels
           - masks geocoronal and other lines.

    Keyword Arguments:
    spec -- [x,y,err] where `x` is in lambda (observed)

    Returns:
    [x,y,err] for fixed spectrum
    """
    dat = np.array(spec).T.copy()

    # Fix zero error points
    mask = dat[:, 2] <= 0
    bigerror = np.median(dat[~mask, 2]) * 1e10
    dat[mask, 2] = bigerror

    # Fix geocoronal and other lines
    # see https://bitbucket.org/lya_ita/lasd/issues/101/mask-geocoronal-lines
    for (am, bm) in [(1214.5, 1216.9), (1301, 1303.2), (1303.7, 1306.9)]:
        mask = (dat[:, 0] > am) & (dat[:, 0] < bm)
        dat[mask, 2] = bigerror

    return dat.T
