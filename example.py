"""Example usage of the LASD analysis pipeline.
"""
import numpy as np

from pprint import pprint
import logging
import os
import sys

p = os.path.abspath("..")
if p not in sys.path:
    sys.path.append(p)

from lasd_analysis.initial_filtering import check_spectrum_properties
from lasd_analysis import run_analysis

# Setup logger
fmt = '[%(levelname)s] %(asctime)-8s %(message)s'
logging.basicConfig(level=logging.INFO,  # Change to DEBUG for more info
                    format=fmt,
                    datefmt='%H:%M:%S')
del fmt

def main(fn, z, R):
    """Example of analysis pipeline.

    Keywords:
      fn    -- Filename of spectrum
      z     -- Approximate redshift
      R     -- Spectral resolution
    """
    spec = np.loadtxt(fn).T # Wavelength, elux, error

    # Basic check if we can analyze the spectrum
    passed, msg = check_spectrum_properties(spec[0], spec[1], spec[2], z)
    assert passed, msg

    # Run analysis with automatic redshift detection and given redshift
    test_modes = [('z_sys', {}), ('z_estimated', {'estimate_z' : True})]
    for clbl, copt in test_modes:
        logging.info("Running analysis for `%s`, mode `%s` (%s)", fn, clbl, str(copt))
        # niter is the number of times the measurements are done (on the `shuffled` spectra)
        # z_err is the redshift uncertainty, only important if z is not automatically obtained
        andict = run_analysis(spec,z, R, niter = 100, check = True, z_err=1e-5, **copt)
        logging.info("Analysis successful.")

        # Analysis will return dictionary of measured values.
        # Each key four values:
        #    (1) the `original` value measured with the unmodified spectrum
        #    (2-4) the 16th, 50th, 84th percentile measured with the `shuffled` spectra. 
        pprint(andict)

    logging.info("Done analyzing `%s`", fn)

if __name__ == '__main__':
    main("dat_example/lars02_cos_esPois.coarse.bin6.ascii", 0.029836,5000)



