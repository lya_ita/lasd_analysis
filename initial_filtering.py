
import numpy as np
import logging
from scipy.special import erf
import scipy.interpolate
import matplotlib.pyplot as plt

from .constants import lambda_lya
from .constants import c

from .detect_peaks import detect_peaks

from .spectra_modification import convert_spectral_units, remove_continuum
from .spectra_helpers import (
    _get_ymax,
    get_F_lc,
    _clip_percentile,
    _clip,
    _linear_left,
    _weighted_quantile,
)
from .continuum_estimation import get_F_cont_iterative, get_F_cont_window


def _get_ymax(y, err):
    """Obtain maximum in `y` taking the error `err` into account.
    """
    m = err < 1e3 * np.median(err)
    return y[m].max()


def check_emitter(
    spec, lambda_buffer=2500, lambda_check=500., sigma_peak=2.5, plot=False
):
    """Check if the spectrum is actually an emitter.

    Keyword Arguments:
    spec           -- [x,y,err]. x in km/s, other units don't matter.
    lambda_buffer  -- Only checks region [-lambda_buffer,lambda_buffer] around Lya
    lambda_check   --  number of AA we take for means (default 15)
    sigma_peak     -- (default 2.5)

    Returns:
    True if the spectrum indicated an emitter, false for absorber
    """

    wl, fl, er = spec
    assert len(wl) == len(fl)
    assert len(er) == len(fl)
    ncheck = int(np.ceil(lambda_check / (wl[1] - wl[0])))

    lya_region = (wl > -1 * lambda_buffer) & (wl < lambda_buffer)

    myw = 1. / np.concatenate(
        [er[lya_region][:ncheck], er[lya_region][-ncheck:]]
    )
    Fside = _weighted_quantile(
        np.concatenate([fl[lya_region][:ncheck], fl[lya_region][-ncheck:]]),
        0.5,
        myw,
    )

    n = int(np.sum(lya_region) / 2)
    myw = 1. / er[lya_region][(n - ncheck) : (n + ncheck)]
    Fcenter = _weighted_quantile(
        fl[lya_region][(n - ncheck) : (n + ncheck)], .5, myw
    )
    Fmax = _get_ymax(
        fl[lya_region][(n - ncheck) : (n + ncheck)],
        er[lya_region][(n - ncheck) : (n + ncheck)],
    )
    medianerr = np.median(er[er < 1e3 * np.median(er)])

    if plot:
        plt.errorbar(wl[lya_region], fl[lya_region], yerr=er[lya_region])
        plt.axhline(Fside, color="k", ls="--")
        plt.axhline(Fmax, color="blue", ls="--")
        plt.axhline(Fcenter, color="green", ls="--")

        plt.ylim(fl[lya_region].min() * 0.9, fl[lya_region].max() * 1.1)

    if (Fcenter < Fside) and (Fmax < Fside + 2 * medianerr):
        return False

    return True


def check_spectrum_properties(
    wl, flux, err, redshift, snr_window_size=250, snr_threshold=7
):
    """ Performs a set of checks of the properties of the spectrum to make sure it
    should be included in the DB. Specifically the following checks are made:
     - Whether the flux and error vectors seems sane (not to many 0 values)
     - Signal-to-noise ratio
     - Whether it is an absorber spectrum
     - Whether it has a lya peak within ±1000 km/s
     - Whether total luminosity is reasonable (< 1e46)

    Parameters
    ----------
    wl : np.array
        Wavelength array
    flux : np.array
        Flux array
    err : np.array
        Error array
    redshift : float
        Redshift, used for conversion to velocities
    snr_window_size : float (optional)
        Sliding window over which to calculate the SNR
    snr_threshold : float
        The minimum signal to noise

    Returns
    -------
    passed : bool
        Boolean value indicating whether all checks passed
    msg : str
        Contains the error message if any checks failed, otherwise ""
    """
    passed = True
    msg = ""
    wl = np.asarray(wl)
    flux = np.asarray(flux)
    err = np.asarray(err)
    try:
        wl_v, flux_v, err_v = convert_spectral_units([wl, flux, err], redshift)
    except Exception as e:
        msg = "Could not convert spectrum to velocities. Server reported ".format(
            e
        )
        passed = False
        return passed, msg

    passed, msg = check_lya_region(wl_v, flux_v, err_v)
    if passed is False:
        return passed, msg

    wl_conv = wl / (1 + redshift)
    passed = check_emitter([wl_conv, flux, err])
    if passed is False:
        msg = "No peak detected. Is this an absorber?"
        return passed, msg

    passed, msg = check_SNR(wl_v, flux_v, err_v)
    if passed is False:
        return passed, msg

    passed, msg = check_lya_luminosity(wl_v, flux_v, err_v)
    if passed is False:
        return passed, msg

    return passed, msg


def check_peak_position(wl, flux, err):
    """ Checks that the peak position is within 1000 km/s of the redshift
    TODO: MORE STABLE ALGORITHM NEEDED

    Parameters
    ----------
    wl : np.array
        wavelength array in velocity units
    flux: np.array
        Flux array (F_v)
    err: np.array
        Error array (F_v)
    Returns
    -------
    passed : bool
    msg : str
    """
    passed = True
    msg = ""
    if not (-1000 < wl[np.argmax(flux)]) & (1000 > wl[np.argmax(flux)]):
        passed = False
        msg = "No peak found in this spectrum"
    return passed, msg


def check_lya_region(wl, flux, err):
    """ Do simple sanity checks on the region around the lya line
    Parameters
    ----------
    wl : np.array
        wavelength array in velocity units
    flux: np.array
        Flux array (F_v)
    err: np.array
        Error array (F_v)
    """
    passed = True
    msg = ""
    lya_region = np.where((wl > -2500) & (wl < 2500))
    central_lya_region = np.where((wl > -300) & (wl < 300))
    nmaxfail = 0.2 * np.sum(lya_region)
    n_fail_central = 2

    # Check total number of identically zero points
    if np.sum(np.array(flux)[lya_region] == 0) > nmaxfail:
        passed = False
        msg += "Too many fluxes are exactly 0.\n"

    # Check number of identically zero points in the line region
    if np.sum(np.array(flux)[central_lya_region] == 0) >= n_fail_central:
        passed = False
        msg += "Detected zerofluxes inside the line. "
        msg += "Spectra with chipgaps or detector failures inside the lya line cannot be accepted for analysis"

    if np.sum(np.array(err)[lya_region] > 1e3 * np.median(err)) > nmaxfail:
        passed = False
        msg += "Too often the errors on the flux are too large.\n"

    return passed, msg


def check_lya_luminosity(wl, flux, err, threshold=1e46):
    """ Checks that the luminosity is sane
    wl : np.array
        wavelength array in velocity units
    flux: np.array
        Flux array (F_nu)
    err: np.array
        Error array (F_nu)
    """
    passed = True
    msg = ""
    window = np.where((wl > -2500) & (wl < 2500))
    wl_w = wl[window]
    flux_w = flux[window]
    err_w = err[window]
    # Subtract continuum
    wl_c, flux_c, err_c = subtract_continuum(wl_w, flux_w, err_w)
    L_tot = np.trapz(flux[window], x=wl_c)
    if L_tot > threshold:
        passed = False
        msg += "Total luminosity is too large (>1e46 erg/s)."
    return passed, msg


def check_SNR(wl, flux, err, window_size=250, threshold=7):
    """ Calculates the signal-to.noise ratio in each pixel in a sliding
    window over the region -2500 to 2500 km/s around lyman alpha
    Parameters
    ----------
    wl : np.array
        Wavelength array in velocity
    flux : np.array
        Flux array (F_v)
    err : np.array
        Error array (In F_v)
    window_size : float (optional)
        Sliding window over which to calculate the SNR
    threshold : float
        The minimum signal to noise
    Return
    -------
    passed : bool
        Whether the SNR is sufficient to accept the spectrum for further
        analysis
    msg : str
        error message (empty string if no error)
    """
    msg = ""
    # get spectral window
    window = np.where((wl > -2500) & (wl < 2500))
    wl_w = wl[window]
    flux_w = flux[window]
    err_w = err[window]
    # Subtract continuum
    wl_c, flux_c, err_c = subtract_continuum(wl_w, flux_w, err_w)
    # Remove absorption
    wl_cr, flux_cr, err_cr = remove_absorption(wl_c, flux_c, err_c)
    # Calculate the SNR in a running median
    snr = running_window_snr(wl_cr, flux_cr, err_cr, window_size=window_size)
    # Check if any of these are above the threshold
    if np.max(snr > threshold):
        passed = True
    else:
        passed = False
        msg = "No peak with S/N greater than 7 was found in the spectrum"
    return passed, msg


def subtract_continuum(wl, flux, err):
    """ Runs the necessary functions to subtract continuum from analysis package
    """
    continuum, cwa, cwb = get_F_cont_window((wl, flux, err), return_window=True)
    y_cr = remove_continuum((wl, flux, err), continuum, cwa, cwb)
    return wl, y_cr, err


def remove_absorption(wl, flux, err, sigma_threshold=2):
    """ Sets all flux values more than 2 sigma below 0 to 0.
    This will slightly overestimate the SNR
    """
    std = np.std(flux)
    flux_cut = flux
    flux_cut[flux_cut < -2 * std]
    return wl, flux_cut, err


def get_snr(flux, err):
    """ Calculates the signal to noise ratio for a spectrum
    Parameters
    ----------
    flux : np.array
    err : np.array

    Returns
    -------
    snr : float
        The total signal to noise ratio for the given flux and error vectors
    """
    signal = np.sum(flux)
    error = np.sqrt(np.sum(err ** 2))
    return signal / error


def running_window_snr(wl, flux, err, window_size=250):
    """ Calculates the SNR in a running window
    Parameters
    ----------
    wl : np.array
    flux : np.array
    err : np.array
    window_size : float (optional)

    Returns
    -------
    snr_array : np.array
        Array of signal to noise for each pixel for a running window
    """
    snr_array = []
    for lam in wl:
        window = np.where(
            (wl > (lam - window_size / 2)) & (wl < (lam + window_size / 2))
        )
        snr_array.append(get_snr(flux[window], err[window]))
    return np.array(snr_array)


def _weighted_quantile(
    values, quantiles, sample_weight=None, values_sorted=False, old_style=False
):
    """ Very close to numpy.percentile, but supports weights.
    NOTE: quantiles should be in [0, 1]!

    From https://stackoverflow.com/questions/21844024/weighted-percentile-using-numpy

    :param values: numpy.array with data
    :param quantiles: array-like with many quantiles needed
    :param sample_weight: array-like of the same length as `array`
    :param values_sorted: bool, if True, then will avoid sorting of
        initial array
    :param old_style: if True, will correct output to be consistent
        with numpy.percentile.
    :return: numpy.array with computed quantiles.
    """
    values = np.array(values)
    quantiles = np.array(quantiles)
    if sample_weight is None:
        sample_weight = np.ones(len(values))
    sample_weight = np.array(sample_weight)
    assert np.all(quantiles >= 0) and np.all(
        quantiles <= 1
    ), "quantiles should be in [0, 1]"

    if not values_sorted:
        sorter = np.argsort(values)
        values = values[sorter]
        sample_weight = sample_weight[sorter]

    weighted_quantiles = np.cumsum(sample_weight) - 0.5 * sample_weight
    if old_style:
        # To be convenient with numpy.percentile
        weighted_quantiles -= weighted_quantiles[0]
        weighted_quantiles /= weighted_quantiles[-1]
    else:
        weighted_quantiles /= np.sum(sample_weight)
    return np.interp(quantiles, weighted_quantiles, values)


def get_F_cont_window(
    spec,
    sigma_thresh=5.0,
    buff=100.0,
    verbose=False,
    step=50.0,
    return_window=False,
    plot=False,
):
    """Detect continuum level by removing a certain area (around the peak) from
    the spectrum and then taking the median value.

    Keyword Arguments:
    spec         -- x, y, err (in velocity space!)
    sigma_thresh -- Threshold for initial peak detection. (default 8.0)
    buff         -- Safety buffer around the area in km/s. (default 100.)
    step         -- Step to take around peak until hitting the continuum in km/s
    """
    x, y, err = spec
    assert x[0] < 0 and x[-1] > 0, "Please provide spectrum in velocity space!"
    if plot:
        plt.clf()
        plt.errorbar(x, y, yerr=err, label="Data")

    # Prep
    m1 = np.abs(x) < 1000.  # to ignore "peaks" far away
    m2 = y[m1] == np.max(y[m1][err[m1] < 1e3 * np.median(err)])
    xmax = x[m1][m2][0]

    # x = x.copy() - xmax
    errmean = 2 * np.median(err)
    # ymean = np.median(y[np.abs(x) > 2 * buff])
    ymean = get_F_cont_iterative(spec)
    if plot:
        plt.axhline(ymean, ls=":", label="First (iterative guess)")

    thresh = sigma_thresh * errmean
    dx = x[1] - x[0]
    istep = int(np.ceil(step / dx))
    msg = " thresh = %g, xmax = %g, ymean = %g, errmean = %g" % (
        thresh,
        xmax,
        ymean,
        errmean,
    )

    # Find central region(s) clearly above continuum
    fcheck = lambda i: np.all(y[i : (i + istep)] - ymean < thresh) and np.abs(
        x[i]
    ) > np.abs(xmax)
    for i in range(np.sum(x < 0), 0, -1):
        if fcheck(i - istep):
            break
    aup = x[i]
    for i in range(np.sum(x < 0), len(x)):
        if fcheck(i):
            break
    bup = x[i]
    msg += ", aup = %g, bup = %g" % (aup, bup)
    if aup < x[5] or bup > x[-5]:
        logging.error(
            "Could not detect central region in continuum detection." + msg
        )
        raise Exception("Failed continuum detection." + msg)
    if plot:
        plt.axvline(aup, c="k", ls="--", alpha=0.5, label="Window (center)")
        plt.axvline(bup, c="k", ls="--", alpha=0.5)

    # Find neighboring points which are ~ at the continuum
    cont = np.median(y[(x < aup) | (x > bup)])
    fcheck = lambda i: np.abs(y[i] - cont) < errmean
    for i in range(np.sum(x < 0), 0, -1):
        if x[i] < aup and fcheck(i):
            break
    adown = x[i]
    for i in range(np.sum(x > 0), len(x)):
        if x[i] > bup and fcheck(i):
            break
    bdown = x[i]
    msg += ", adown = %g, bdown = %g" % (adown, bdown)
    if adown < x[2] or bdown > x[-2]:
        logging.error("Problems refining region in continuum detection." + msg)
        if plot:
            plt.clf()
            plt.errorbar(x, y, yerr=err)
            plt.axvline(aup)
            plt.axvline(bup)
            plt.axhline(ymean)
            logging.error("Plotted failure in `fail.png`.")
            plt.savefig("fail.png")
        raise Exception("Failed continuum detection." + msg)

    ax, bx = adown - buff, bdown + buff

    if plot:
        plt.axvline(ax, c="k", ls="-.", alpha=0.7, label="Window (cont.)")
        plt.axvline(bx, c="k", ls="-.", alpha=0.7)

    # Get continuum within this range
    m = np.logical_or(x < ax, x > bx)
    if np.sum(m) < 5:
        logging.error(
            "Could not detect continuum. Too few continuum data points? "
            "We're trying to estimate the continuum in the range"
            " <%f and >%f and only %d points fall into this.",
            ax,
            bx,
            np.sum(m),
        )
        raise Exception("Failed continuum detection.")
    # cont = np.median(y[m])
    m2 = m & (np.abs(y - cont) < 5 * errmean)
    # cont = np.median(y[m2])
    cont = _weighted_quantile(
        y[m2], 0.5, 1.0 / err[m2]
    )  # Take the error into account.

    if verbose:
        logging.info(
            "Continuum detected with window method: %g (from %g to %g not used)",
            cont,
            ax,
            bx,
        )
    if plot:
        plt.axhline(cont, ls="--", alpha=0.5, label="Cont. (final)")
        plt.legend(loc="best", fontsize="small")

    # Checks
    assert not np.isnan(cont), "Continuum is nan."

    if return_window:
        return cont, ax, bx
    else:
        return cont
