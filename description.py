"""Description of all the quantities.
"""

DESCRIPTION = {
    "Dx_max" : {"description" : "Peak separation between maximum luminosity densities.",
                "units" : "km/s"},
    "Dx_mean" : {"description" : "Peak separation between first moments of both sides.",
                 "units" : "km/s"},
    "EW" : {"description" : "Equivalent width of line. Value of -1000000 is a flag for undetected continuum level.",
            "units" : "Å"},
    "FWHM_max" : {"description" : "Full-width at half maximum of highest peak.",
                  "units" : "km/s"},
    "FWHM_neg" : {"description" : "Full-width at half maximum of blue side.",
                  "units" : "km/s"},
    "FWHM_pos" : {"description" : "Full-width at half maximum of red side.",
                  "units" : "km/s"},
    "F_cont" : {"description" : "Level of continuum.",
                "units" : "erg/s/(km/s)"},
    "F_lc" : {"description" : "Luminosity density at line center.",
              "units" : "erg/s/(km/s)"},
    "F_max" : {"description" : "Luminosity density of highest peak.",
               "units" : "erg/s/(km/s)"},
    "F_neg_max" : {"description" : "Luminosity density of highest peak on blue side.",
                   "units" : "erg/s/(km/s)"},
    "F_pos_max" : {"description" : "Luminosity density of highest peak on red side.",
                   "units" : "erg/s/(km/s)"},
    "F_valley" : {"description" : "Luminosity density of minimum between peaks.",
                  "units" : "erg/s/(km/s)"},
    "L_neg" : {"description" : "Luminosity of blue side.",
               "units" : "erg/s"},
    "L_pos" : {"description" : "Luminosity of red side.",
               "units" : "erg/s"},
    "L_tot" : {"description" : "Total luminosity.",
               "units" : "erg/s"},
    "R_F_cut_neg" : {"description" : "Ratio of maximum luminosity density and peak detection threshold on blue side.",
                     "units" : ""},
    "R_F_cut_pos" : {"description" : "Ratio of maximum luminosity density and peak detection threshold on blue side.",
                     "units" : ""},
    "R_F_lc_max" : {"description" : "Ratio of luminosity density at line center and maximum peak height.",
                    "units" : ""},
    "R_F_pos_neg" : {"description" : "Ratio of luminosity density at red and blue peak.",
                     "units" : ""},
    "R_F_valley_max" : {"description" : "Ratio of luminosity density in the `valley` between the peaks and the maximum peak.",
                        "units" : ""},
    "R_L_cut_neg" : {"description" : "Ratio of blueward luminosity and peak detection threshold.",
                     "units" : ""},
    "R_L_cut_pos" : {"description" : "Ratio of redward luminosity and peak detection threshold.",
                     "units" : ""},
    "R_L_pos_neg" : {"description" : "Ratio of redward over blueward luminosity.",
                     "units" : ""},
    "W_std" : {"description" : "Square-root of second moment of whole spectrum.",
                   "units" : "km/s"},
    "W_neg_std" : {"description" : "Blue peak width as measured by square-root of second moment.",
                   "units" : "km/s"},
    "W_pos_std" : {"description" : "Red peak width as measured by square-root of second moment.",
                   "units" : "km/s"},
    "neg_peak_fraction" : {"description" : "Fraction of times a blue peak was detected.",
                           "units" : ""},
    "pos_peak_fraction" : {"description" : "Fraction of times a red peak was detected.",
                           "units" : ""},
    "skew" : {"description" : "Pearson's moment coefficient of skewness of whole spectrum.",
              "units" : ""},
    "skew_neg" : {"description" : "Pearson's moment coefficient of skewness of blue side.",
                  "units" : ""},
    "skew_pos" : {"description" : "Pearson's moment coefficient of skewness of red side.",
                  "units" : ""},
    "x_max" : {"description" : "Highest peak position determined by maximum luminosity density.",
               "units" : "km/s"},
    "x_mean" : {"description" : "First moment of spectrum.",
               "units" : "km/s"},
    "x_neg_max" : {"description" : "Peak position determined by maximum luminosity density on blue side.",
                   "units" : "km/s"},
    "x_neg_mean" : {"description" : "Peak position determined by weighted mean on blue side.",
                    "units" : "km/s"},
    "x_pos_max" : {"description" : "Peak position determined by maximum luminosity density on red side.",
                   "units" : "km/s"},
    "x_pos_mean" : {"description" : "Peak position determined by weighted mean on red side.",
                    "units" : "km/s"},
    "x_valley" : {"description" : "Position of `valley` between the peaks.",
                  "units" : "km/s"},
    "z" : {"description" : "Systemic redshift of source.",
           "units" : ""}

}


def export_description(form = 'html'):
    import pandas
    # Switch off truncation of strings
    # https://stackoverflow.com/questions/26277757/pandas-to-html-truncates-string-contents
    pandas.set_option('display.max_colwidth', -1)
    df = pandas.DataFrame(DESCRIPTION).T

    if form == 'html':
        print(df.to_html())
    else:
        raise ValueError("Unknown format `%s`." %(form))

if __name__ == '__main__':
    export_description()
