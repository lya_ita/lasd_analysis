"""Functions for spectra analysis, i.\e., they take a spectrum and return some
extracted quantity.
"""
import numpy as np
import logging
from scipy.special import erf
import scipy.interpolate
import matplotlib.pyplot as plt

from .constants import lambda_lya
from .constants import c

from .detect_peaks import detect_peaks

from .spectra_modification import convert_spectral_units, remove_continuum
from .initial_filtering import check_emitter

from .spectra_helpers import (
    _get_ymax,
    get_F_lc,
    _clip_percentile,
    _clip,
    _linear_left,
    _weighted_quantile,
)

from .continuum_estimation import get_F_cont_iterative, get_F_cont_window

get_F_cont = get_F_cont_window


def analyze_spectrum(
    spec,
    run_peak_detection=False,
    delta_cut_F=5.0,
    delta_cut_L=5.0,
    verbose=False,
    check=False,
):
    """Analyzes spectrum. Returns dictionary of properties (see below)

    Keyword Arguments:
    spec           -- x,y and (optional) error values
    run_peak_detection -- Perform peak detection and save peaks & valleys
    delta_cut_*      -- Will not accept peak if maximum is below
                        `delta_cut` * (average error) or integrated luminosity
                       correspondingly.
    check        -- If `True` (default: `False`) will check analysis values
                    before returning.

    Returns:
    Dictionary with spectral properties. General conventions:
    - pos/neg are for positive / negative side of spectrum (in terms of wavelength)
      This means if the wavelength units are km/s pos corresponds to `redward`
    - F_ is prefix for flux (in input units...commonly normalized to unity)
    - x_ prefix for wavelength (in input units)
    - L_ for integrated flux (\int F * dx)
    - R_ for ratios of quantities, often followed by L or F

    Specifically these quantities are returned (only `pos` versions are listed):
    F_lc            -- Flux at line center
    F_max           -- Maximum flux
    F_[pos/neg]_max -- Maximum flux on the positive side
                       (in terms of wavelength units)
    x_[pos/neg]_max -- Wavelength of maximum flux on positive side
    L_pos           -- Integrated positive flux
    R_F_lc_max      -- Ratio of flux at line center to maximum flux
    R_F_pos_neg     -- Ratio of maximum flux on positive to negative side
    F_valley        -- Minimum flux between `x_neg_max` and `x_pos_max`
    FWHM_max        -- Full with at half maximum of maximum peak.
    """
    r = {}

    x, y, err = spec[0], spec[1], spec[2]

    dx = np.concatenate(
        [[x[1] - x[0]], 0.5 * (x[2:] - x[:-2]), [x[-1] - x[-2]]]
    )

    m_pos = x >= 0.0
    m_neg = x <= 0.0
    two_peaks = True
    avg_err = np.median(err)

    ## Global things
    r["F_lc"] = get_F_lc(spec)
    r["F_cont"], cwa, cwb = get_F_cont(spec, return_window=True)

    ## Removed fluxes for moments. values < 0 wont work (weights)
    y_cr = remove_continuum(spec, r["F_cont"], cwa, cwb)

    ymax = _get_ymax(y, err)
    r["F_max"] = ymax
    r["x_max"] = np.mean(x[y == ymax])
    r["L_tot"] = np.sum(y_cr * dx)
    r["EW"] = r["L_tot"] / r["F_cont"]
    r["EW"] = r["EW"] / (1 / lambda_lya * c / 1e5)  # convert to AA
    if r["EW"] < 0:
        r["EW"] = -1e6

    L_cut = delta_cut_L * avg_err * np.mean(dx)
    F_cut = delta_cut_F * avg_err + r["F_cont"]

    ## Quantities on positive / negative side
    for (cm, clbl) in [(m_pos, "pos"), (m_neg, "neg")]:
        errormsg = "Analysing spectrum failed. Data so far: %s." % (str(r))
        if np.sum(y_cr[cm]) < 0:
            errormsg += " No data on `%s` side." % (clbl)
            logging.error(errormsg)
            raise Exception(errormsg)

        # Fluxes
        r["F_%s_max" % (clbl)] = _get_ymax(y[cm], err[cm])

        # Integrated fluxes
        r["L_%s" % (clbl)] = np.sum(y_cr[cm] * dx[cm])

        # Also save cut ratios.
        r["R_L_cut_%s" % (clbl)] = r["L_%s" % (clbl)] / L_cut
        r["R_F_cut_%s" % (clbl)] = r["F_%s_max" % (clbl)] / F_cut

        if verbose:
            logging.info(
                (
                    "%s: L / L_cut = %.2f, F / F_cut = %.2f"
                    % (clbl, r["R_L_cut_%s" % (clbl)], r["R_F_cut_%s" % (clbl)])
                )
            )

        # Check if there is a blue / red peak. If not skip rest for this side
        if (
            r["L_%s" % (clbl)] < L_cut
            or r["F_%s_max" % (clbl)] < F_cut
            or r["F_%s_max" % (clbl)] < 3 * np.median(err)
            or _get_ymax(y_cr[cm][1:-1], err[cm][1:-1]) < 3 * np.median(err)
        ):
            if verbose:
                logging.info((("SKIP %s" % (clbl))))
            two_peaks = False
            continue

        # Moments
        S = np.sum(y_cr[cm])
        mu = np.sum(y_cr[cm] * x[cm]) / S
        sd = np.sqrt(np.sum(y_cr[cm] * x[cm] ** 2) / S - mu ** 2)
        r["x_%s_mean" % (clbl)] = mu
        r["W_%s_std" % (clbl)] = sd
        ## Pearson's moment coefficient of skewness
        r["skew_%s" % (clbl)] = np.sum(((x[cm] - mu) / sd) ** 3 * y_cr[cm]) / S

        # Position of fluxes / peaks
        r["x_%s_max" % (clbl)] = np.mean(x[y == r["F_%s_max" % (clbl)]])

        # FWHM
        r["FWHM_%s" % (clbl)] = get_FWHM([x[cm], y_cr[cm], err[cm]])

    ## Derived quantities
    # Separation
    if two_peaks:
        r["Dx_max"] = r["x_pos_max"] - r["x_neg_max"]
        r["Dx_mean"] = r["x_pos_mean"] - r["x_neg_mean"]

    # Ratios
    r["R_F_lc_max"] = r["F_lc"] / r["F_max"]
    if two_peaks:
        r["R_F_pos_neg"] = r["F_pos_max"] / r["F_neg_max"]
        r["R_L_pos_neg"] = r["L_pos"] / r["L_neg"]

    # Other TODO: need to check if useful
    if two_peaks:
        r["F_valley"] = np.min(
            y[
                (x <= r["x_pos_max"] + np.mean(dx))
                & (x >= r["x_neg_max"] - np.mean(dx))
            ]
        )
        r["x_valley"] = np.mean(x[y == r["F_valley"]])
        r["R_F_valley_max"] = r["F_valley"] / r["F_max"]
    r["FWHM_max"] = get_FWHM([x, y_cr, err])

    # Moments of full spectrum
    S = np.sum(y_cr)
    mu = np.sum(y_cr * x) / S
    sd = np.sqrt(np.sum(y_cr * x ** 2) / S - mu ** 2)
    r["x_mean"] = mu
    r["W_std"] = sd
    ## Pearson's moment coefficient of skewness
    r["skew"] = np.sum(((x - mu) / sd) ** 3 * y_cr) / S

    ## Peak detection stuff
    if run_peak_detection:
        peaklst, vallst = detect_peaks(spec)
        r["peaks"] = peaklst
        r["valleys"] = vallst

    if check:
        check_andict(r)

    return r


def check_andict(d0):
    d = {}
    fail = False
    for k, v in d0.items():
        d[k] = np.array(v)
        try:
            if np.any(np.isnan(d[k][1:])):
                fail = True
                logging.error("nan found in %s --> %s", k, str(d[k]))
        except:
            pass
    if "x_neg_mean" in d and not np.any(np.isnan(d["x_neg_mean"])):
        if np.any(d["x_neg_mean"] > 0):
            logging.error(
                "`x_neg_mean` not negative but `%s`!" % (str(d["x_neg_mean"]))
            )
            fail = True

    if "x_pos_mean" in d and not np.any(np.isnan(d["x_pos_mean"])):
        if np.any(d["x_pos_mean"] < 0):
            logging.error(
                "`x_pos_mean` not positive  but `%s`!" % (str(d["x_pos_mean"]))
            )
            fail = True

    # if np.any(d['EW'] < 0) and d['EW'] != 1.0:
    #     logging.error("Negative EW (%g) found.", d['EW'])
    #     fail = True

    if fail:
        raise Exception("Spectral check failed.")


def get_FWHM(spec):
    """Determine full-with-half-maximum of a peaked set of points.
    """
    x, y = spec[0], spec[1].copy()
    # TODO: Fix this.
    y[-1] = 0.0
    y[0] = 0.0

    # Do some checks first because this failed in the past
    assert not np.any(np.isnan(y)), "nan found."
    assert not np.max(y) == np.min(y), "y is single valued."

    ymax = _get_ymax(y, spec[2])
    half_max = ymax / 2.0
    x_max = np.mean(x[y == ymax])
    s = scipy.interpolate.splrep(x, y - half_max, k=3)
    roots = scipy.interpolate.sproot(s)

    # Sometimes sproot fails for some reason...?
    if len(roots) < 2 or roots[0] > x_max or roots[-1] < x_max:
        logging.debug(
            "Root finding via sproot failed (x_max=%g, roots = %s)",
            x_max,
            str(roots),
        )
        f = scipy.interpolate.interp1d(x, y - half_max)
        roots = [
            scipy.optimize.brentq(f, np.min(x), x_max),
            scipy.optimize.brentq(f, x_max, np.max(x)),
        ]

    if len(roots) < 2:
        raise Exception(
            "No peaks found!? Is the data constant or one-sided...?"
        )
    if len(roots) == 2:
        return abs(roots[1] - roots[0])

    # Otherwise find solutions closest to peak
    # if hasattr(x_max, "__len__"):
    #     for cx in x_max:
    #         if cx > np.min(roots) and cx < np.max(roots):
    #             break
    #         else:
    #             raise ValueError("Problem finding FWHM. x_max = %s, roots = %s",
    #                             str(x_max), str(roots))
    #             return np.nan
    #     x_max = cx
    d = x_max - roots
    return np.min(-d[d <= 0]) + np.min(d[d >= 0])


def get_FWHM_at_peaks(spec, peaklst, vallst, plot_line=False):
    """Calculates the full widht at half maximum of every given peak.

    Keyword Arguments:
    spec    -- x,y, err
    peaklst -- List of peaks
    vallst  -- List of valleys

    Returns:
    List of FWHM values in units of `x`.
    """
    x, y, err = spec
    xpeaks, ypeaks = peaklst
    fwhm = []
    eps = (x[1] - x[0]) * 1e-3

    for i in range(len(xpeaks)):
        xp = np.copy(xpeaks[i]) + eps
        yp = np.copy(ypeaks[i])
        hm = 0.5 * yp

        # Find left crossing
        leftm = (y[1:] >= hm) & (y[:-1] <= hm)
        assert (
            np.sum(leftm) > 0
        ), "At least something has to cross the HW line...?"
        xleft = x[:-1][leftm]
        xleft = xleft[xleft <= xp]
        xleft = xleft[np.min(xp - xleft) == xp - xleft][0]
        xintp_left = _linear_left(x, y, xleft, hm)

        # Find right crossing
        xp -= 2 * eps
        rightm = (y[1:] <= hm) & (y[:-1] >= hm)
        assert (
            np.sum(rightm) > 0
        ), "At least something has to cross the HW line to the right!?"
        xright = x[:-1][rightm]
        xright = xright[xright >= xp]
        xright = xright[np.min(xright - xp) == xright - xp][0]
        xintp_right = _linear_left(x, y, xright, hm)

        fwhm.append(xintp_right - xintp_left)

        if plot_line:
            plt.plot([xintp_left, xintp_right], [hm, hm], "-")

    return fwhm
