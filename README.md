Analysis pipeline for the "Lyman Alpha Spectral Database" (LASD)

The code provided is to be able to analyze synthetic spectra (e.g., from radiative transfer models) in the same way as done in LASD. If you have *observed* spectra, please consider [uploading them instead](http://lasd.lyman-alpha.com/upload). LASD only works with input from the community!

Usage: see `example.py`

More infos, terms of use, and documentation on <http://lasd.lyman-alpha.com>.
