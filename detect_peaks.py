import numpy as np
import scipy.interpolate
import matplotlib.pyplot as plt

from .peakdetect import peakdetect
from .spectra_modification import smooth_spectrum
from .spectra_modification import get_smoothing_kernel


def _most_common(lst):
    return max(set(lst), key=lst.count)


def detect_peaks(
    spec0,
    searchlength=list(range(4, 15)),
    delta=2.5,
    remove_outer_valleys=True,
    min_peakwidth=7,
    minsep=50,
    maxsep=1200,
    smooth_spectrum_width=False,
    smooth_width_unit="v",
    R=None,
    plot=False,
    verbose=False,
):
    """Detects peaks in spectrum.
    
    Parameters
    ----------
    spec : list or tuple
        List with [xvals,vvals, yerr]
    searchlength : list
        lookaheads (in px) used by peak detection
    delta : float
        his specifies a minimum difference between a peak and
        the following points, before a peak may be considered a peak.
    remove_outer_valleys : bool
        Drops valleys which are not between maxima.
    min_peakwidth : float
        No of datapoints which should be above error for peak to be detected.
    minsep : float
        Specifies minimum allowed separation between two peaks
    maxsep : float
        Specifies minimum allowed separation between two peaks
    smooth_spectrum_width : float or False
        Width of Gaussian smoothing in input wavelength of `xvals`.
        Unit is set by `smooth_width_unit`.
    smooth_width_unit : str ['v', 'px', 'R']
        sets which units smoothing width is given in.
        if 'v' - km/s, 'px' - pixels, 'R' - resolution elements
    R : float
        Resolving power  specified by the user
    plot : bool
        makes a plot of the results
    verbose : bool
        annotates and prints results
    
    Returns
    -------
    [[peakx, peaky], [valleys_x, valleys_y]]
    """
    if smooth_spectrum_width is False:
        spec = spec0.copy()
    else:
        if smooth_width_unit == "R":
            if smooth_spectrum_width:
                if R is not None:
                    kernel = get_smoothing_kernel(
                        spec0, R, multiplicative_factor=smooth_spectrum_width
                    )
        else:
            kernel = smooth_spectrum_width
        spec = smooth_spectrum(spec0, kernel, width_unit=smooth_width_unit)

    if len(spec) != 3:
        raise ValueError("`spec` has to be [xvals,yvals,err] (len 3!)")
    x, y, err = spec
    ndec = None

    errmean = np.sqrt(np.mean(err[err != 0] ** 2))
    rr = [
        peakdetect(y, x, lookahead=i, delta0=j, err=err, min_peakwidth=min_peakwidth)
        for i in searchlength
        for j in np.array([1.0]) * delta
    ]
    npeaks = [len(i[0]) for i in rr]
    nvals = [len(i[1]) for i in rr]

    ndec = int(np.round(np.median(npeaks)))

    if ndec not in npeaks:
        ndec = int(_most_common(npeaks))
    if ndec == 0:  # No peaks detected.
        if verbose:
            print("No peaks detected.")
        return [[], []], [[], []]

    rr = rr[npeaks.index(ndec)]
    # peaks
    peakx, peaky = list(zip(*rr[0]))
    peakx = list(peakx)
    peaky = list(peaky)

    # valleys
    vx = vy = []
    if len(rr[1]):
        vx, vy = list(zip(*rr[1]))
    vx = list(vx)
    vy = list(vy)

    if verbose:
        print("Found %s peaks   --> %d peaks (%s)" % (str(npeaks), ndec, str(peakx)))
        print(
            "Found %s valleys --> %d vals (%s)"
            % (str(nvals), nvals[npeaks.index(ndec)], str(vx))
        )

    # Filter peaks
    # If too far apart or too close, only take the ones close enough to the main peak
    remove_lst = []
    if len(peakx) > 1:
        for i in range(len(peakx)):
            dx = np.abs(peakx[i] - peakx[peaky.index(np.max(peaky))])
            if dx > 0:
                if dx < minsep or dx > maxsep:
                    remove_lst.append(i)
    if len(remove_lst) > 0 and verbose:
        print("Remove peaks %s for seperation issues." % (str(remove_lst)))
    for i in remove_lst[::-1]:
        peakx.pop(i)
        peaky.pop(i)

    if remove_outer_valleys and len(vx) > 0:
        if vx[0] < peakx[0]:
            if verbose:
                print("Remove first valley.")
            vx.pop(0)
            vy.pop(0)
    if remove_outer_valleys and len(vx) > 0:
        if vx[-1] > peakx[-1]:
            if verbose:
                print("Remove last valley.")
            vx.pop(-1)
            vy.pop(-1)

    # Plot if wished
    if plot:
        from .spectra_analysis import get_FWHM_at_peaks

        plt.clf()
        plt.errorbar(x, y, yerr=err)
        [plt.axvline(i) for i in peakx]
        [plt.axvline(i, color="green") for i in vx]

        # Plot also FWHM
        r = get_FWHM_at_peaks(spec, [peakx, peaky], [vx, vy], plot_line=True)

        if plot is True:
            plt.show()
        else:
            if verbose:
                print("Saving figure to", plot)
            plt.savefig(plot)

    return [peakx, peaky], [vx, vy]
