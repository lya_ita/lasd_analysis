
import numpy as np
import logging
from scipy.special import erf
import scipy.interpolate
import matplotlib.pyplot as plt

from .constants import lambda_lya
from .constants import c

from .detect_peaks import detect_peaks

from .spectra_modification import convert_spectral_units, remove_continuum

from .spectra_helpers import (
    _get_ymax,
    get_F_lc,
    _clip_percentile,
    _clip,
    _linear_left,
    _weighted_quantile,
)


def get_F_cont_iterative(
    spec, sigma_threshold=5.0, niter=20, verbose=False, use_median=False
):
    """Get continuum level in an iterative process.
    Will clip `niter` times all the data points above / below `sigma_threshold` sigma
    of the median level.

    Keyword Arguments:
    spec            -- [x,y,err]
    sigma_threshold -- Clip data points which are outside of `sigma_threshold`
                       of the other ones.
    niter           -- Number of times the clipping is repeated
    verbose         -- If true outputs some stats
    """
    x, y, err = spec
    y = y.copy()
    y = y[np.abs(x) > 200.0]

    for i in range(niter):
        y = _clip(y, sigma_threshold)

    if use_median:
        cont = np.median(y)
    else:
        cont = np.mean(y)

    if verbose:
        print(
            "Continuum is %g (using %d of %d data points)"
            % (cont, len(y), len(x))
        )
    return cont


def get_F_cont_window(
    spec,
    sigma_thresh=5.0,
    buff=100.0,
    verbose=False,
    step=50.0,
    return_window=False,
    plot=False,
):
    """Detect continuum level by removing a certain area (around the peak) from
    the spectrum and then taking the median value.

    Keyword Arguments:
    spec         -- x, y, err (in velocity space!)
    sigma_thresh -- Threshold for initial peak detection. (default 8.0)
    buff         -- Safety buffer around the area in km/s. (default 100.)
    step         -- Step to take around peak until hitting the continuum in km/s
    """
    x, y, err = spec
    assert x[0] < 0 and x[-1] > 0, "Please provide spectrum in velocity space!"
    if plot:
        plt.clf()
        plt.errorbar(x, y, yerr=err, label="Data")

    # Prep
    m1 = np.abs(x) < 1000.  # to ignore "peaks" far away
    m2 = y[m1] == np.max(y[m1][err[m1] < 1e3 * np.median(err)])
    xmax = x[m1][m2][0]

    # x = x.copy() - xmax
    errmean = 2 * np.median(err)
    # ymean = np.median(y[np.abs(x) > 2 * buff])
    ymean = get_F_cont_iterative(spec)
    if plot:
        plt.axhline(ymean, ls=":", label="First (iterative guess)")

    thresh = sigma_thresh * errmean
    dx = x[1] - x[0]
    istep = int(np.ceil(step / dx))
    msg = " thresh = %g, xmax = %g, ymean = %g, errmean = %g" % (
        thresh,
        xmax,
        ymean,
        errmean,
    )

    # Find central region(s) clearly above continuum
    fcheck = lambda i: np.all(y[i : (i + istep)] - ymean < thresh) and np.abs(
        x[i]
    ) > np.abs(xmax)
    for i in range(np.sum(x < 0), 0, -1):
        if fcheck(i - istep):
            break
    aup = x[i]
    for i in range(np.sum(x < 0), len(x)):
        if fcheck(i):
            break
    bup = x[i]
    msg += ", aup = %g, bup = %g" % (aup, bup)
    if aup < x[5] or bup > x[-5]:
        logging.error(
            "Could not detect central region in continuum detection." + msg
        )
        raise Exception("Failed continuum detection." + msg)
    if plot:
        plt.axvline(aup, c="k", ls="--", alpha=0.5, label="Window (center)")
        plt.axvline(bup, c="k", ls="--", alpha=0.5)

    # Find neighboring points which are ~ at the continuum
    cont = np.median(y[(x < aup) | (x > bup)])
    fcheck = lambda i: np.abs(y[i] - cont) < errmean
    for i in range(np.sum(x < 0), 0, -1):
        if x[i] < aup and fcheck(i):
            break
    adown = x[i]
    for i in range(np.sum(x > 0), len(x)):
        if x[i] > bup and fcheck(i):
            break
    bdown = x[i]
    msg += ", adown = %g, bdown = %g" % (adown, bdown)
    if adown < x[2] or bdown > x[-2]:
        logging.error("Problems refining region in continuum detection." + msg)
        if plot:
            plt.clf()
            plt.errorbar(x, y, yerr=err)
            plt.axvline(aup)
            plt.axvline(bup)
            plt.axhline(ymean)
            logging.error("Plotted failure in `fail.png`.")
            plt.savefig("fail.png")
        raise Exception("Failed continuum detection." + msg)

    ax, bx = adown - buff, bdown + buff

    if plot:
        plt.axvline(ax, c="k", ls="-.", alpha=0.7, label="Window (cont.)")
        plt.axvline(bx, c="k", ls="-.", alpha=0.7)

    # Get continuum within this range
    m = np.logical_or(x < ax, x > bx)
    if np.sum(m) < 5:
        logging.error(
            "Could not detect continuum. Too few continuum data points? "
            "We're trying to estimate the continuum in the range"
            " <%f and >%f and only %d points fall into this.",
            ax,
            bx,
            np.sum(m),
        )
        raise Exception("Failed continuum detection.")
    # cont = np.median(y[m])
    m2 = m & (np.abs(y - cont) < 5 * errmean)
    # cont = np.median(y[m2])
    cont = _weighted_quantile(
        y[m2], 0.5, 1.0 / err[m2]
    )  # Take the error into account.

    if verbose:
        logging.info(
            "Continuum detected with window method: %g (from %g to %g not used)",
            cont,
            ax,
            bx,
        )
    if plot:
        plt.axhline(cont, ls="--", alpha=0.5, label="Cont. (final)")
        plt.legend(loc="best", fontsize="small")

    # Checks
    assert not np.isnan(cont), "Continuum is nan."

    if return_window:
        return cont, ax, bx
    else:
        return cont
