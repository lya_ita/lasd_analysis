import numpy as np
import logging
from scipy.special import erf
import scipy.interpolate
import matplotlib.pyplot as plt

from .constants import lambda_lya
from .constants import c

from .detect_peaks import detect_peaks

from .spectra_modification import convert_spectral_units, remove_continuum


def _get_ymax(y, err):
    """Obtain maximum in `y` taking the error `err` into account.
    """
    m = err < 1e3 * np.median(err)
    return y[m].max()


def get_F_lc(spec):
    """Returns flux at line center (F(0))
    """
    f = scipy.interpolate.interp1d(spec[0], spec[1])

    return float(f(0))


def _clip_percentile(y, sigma_removal):
    p = 100 * 0.5 * (1 - erf(sigma_removal / np.sqrt(2)))  # sigma to percentile
    pvs = np.percentile(y, [p, 100 - p])
    return y[(y > pvs[0]) & (y < pvs[1])]


def _clip(y, sigma_removal):
    sd = np.std(y) * sigma_removal
    pvs = [np.median(y) + fac * sd for fac in [-1, 1]]
    return y[(y > pvs[0]) & (y < pvs[1])]


def _linear_left(x, y, xleft, y_out):
    """Makes a linear interpolation right of the point `xleft`
    """
    m = x == xleft
    ix = np.arange(len(x))[m][0]

    x_out = (y_out - y[ix]) * (x[ix + 1] - x[ix]) / (y[ix + 1] - y[ix]) + x[ix]

    return x_out


def _weighted_quantile(
    values, quantiles, sample_weight=None, values_sorted=False, old_style=False
):
    """ Very close to numpy.percentile, but supports weights.
    NOTE: quantiles should be in [0, 1]!

    From https://stackoverflow.com/questions/21844024/weighted-percentile-using-numpy

    :param values: numpy.array with data
    :param quantiles: array-like with many quantiles needed
    :param sample_weight: array-like of the same length as `array`
    :param values_sorted: bool, if True, then will avoid sorting of
        initial array
    :param old_style: if True, will correct output to be consistent
        with numpy.percentile.
    :return: numpy.array with computed quantiles.
    """
    values = np.array(values)
    quantiles = np.array(quantiles)
    if sample_weight is None:
        sample_weight = np.ones(len(values))
    sample_weight = np.array(sample_weight)
    assert np.all(quantiles >= 0) and np.all(
        quantiles <= 1
    ), "quantiles should be in [0, 1]"

    if not values_sorted:
        sorter = np.argsort(values)
        values = values[sorter]
        sample_weight = sample_weight[sorter]

    weighted_quantiles = np.cumsum(sample_weight) - 0.5 * sample_weight
    if old_style:
        # To be convenient with numpy.percentile
        weighted_quantiles -= weighted_quantiles[0]
        weighted_quantiles /= weighted_quantiles[-1]
    else:
        weighted_quantiles /= np.sum(sample_weight)
    return np.interp(quantiles, weighted_quantiles, values)
